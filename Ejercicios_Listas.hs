--Ejercicio 1
sumaDeCuadrados:: Integer-> Integer
sumaDeCuadrados n = sum([x^2 | x<-[1..n]])

--Ejercicio 2
emparejarLista l1 l2 = zip l1 l2

--ejercicio 3
palindrome:: String->Bool
palindrome c | c == reverse (c) = True
             | otherwise = False

--Ejercicio 4
pertenece xs a = a `elem` xs

--Ejercicio 5


--Ejercicio 6
cad c i = sum([if x == ' ' then i+0 else i+1 | x<-c])

--Ejercicio 7
divisoresPropios :: Integer -> [Integer]
divisoresPropios n = [a | a <- [1..n-1]
                        , n `mod` a == 0]

esPerfecto :: Integer -> Bool
esPerfecto n = sum (divisoresPropios n) == n

--Ejercicio 8
distance p1 p2 = sqrt((fst(p2) - fst(p1))^2 + (snd(p2) - snd(p1))^2)


--Ejercicio 9
thrdA (a,_,_) = a
thrdB (_,b,_) = b
thrdC (_,_,c) = c
incT t i = "(" ++ show((thrdA (t) * i)) ++ "," ++ show((thrdB(t) * i )) ++ "," ++ show((thrdC(t) * i)) ++ ")"

--Ejercicio 10
incList l i = [x*i | x<-l]









